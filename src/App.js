import React, { Component } from 'react';
import './App.css';
import Websocket from 'react-websocket';
import smstone from './dilin.mp3'
import {
  AppBar,
  Toolbar,
  List, 
  ListItem, 
  ListItemIcon, 
  ListItemText,
  Grid,
  Snackbar,
  IconButton,
  Button,
  Card,
  CardContent,
  Divider
} from "@material-ui/core";
import {Close as CloseIcon} from '@material-ui/icons';
import SvgIcon from '@material-ui/core/SvgIcon';
import { withStyles } from '@material-ui/core/styles';
import logo from './logo.svg';

const styles = theme => ({
  close: {
    padding: theme.spacing.unit / 2,
  },
  cardForm : {
    height:"80vh"
  },
  iconRight : {
    position:"absolute",
    right: 10
  },
  iconLeft : {
    position:"absolute"
  },
  logoImg : {
    height : 50
  },
  messageWidth : {
    width : 150,
    textAlign:"justify"
  },
  snackBarOffset :{
    marginTop:150
  }
});

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      messages:[],
      showMessage:false,
      payload:{},
      requestMessage:""
    }
    this.websocket = React.createRef();
  }

  handleData = (data) => {
    const payload = JSON.parse(data);
    const tone = new Audio(smstone);
    try {
      tone.play();
    } catch (error) {
      console.log(error);
    }
    if(payload.request){
      if(!this.state.showMessage)
        this.setState({
          payload:payload,
          showMessage:true,
          requestMessage:("A payment request of GHS "+payload.request.amount
            +" has been received. Do you want to authorise?")
        })
      else
        this.websocket.current.sendMessage(data);
    } else {
      const addmessages = this.state.messages;
      addmessages.unshift(payload.message);
      this.setState({messages:addmessages});
      this.websocket.current.sendMessage(data);
    }
  }

  handleOk = (event, reason) => {
    const {payload} = this.state;
    payload.action = "yes";
    const data = JSON.stringify(payload);
    this.websocket.current.sendMessage(data);
    this.setState({showMessage:false});
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    const {payload} = this.state;
    payload.action = "no";
    const data = JSON.stringify(payload);
    this.websocket.current.sendMessage(data);
    this.setState({showMessage:false});
  }

  render() {
    const { classes } = this.props;

    const query = new URLSearchParams(window.location.search);

    const mobileNo = query.get("mobileNo")? query.get("mobileNo"):"0212345678";

    return (
      <div className="App">
        <Snackbar
          className={classes.snackBarOffset}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          open={this.state.showMessage}
          autoHideDuration={30000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<div id="message-id" className={classes.messageWidth}>{this.state.requestMessage}</div>}
          action={[
            <Button key="yes" color="primary" size="small"
              onClick={this.handleOk}>
                Yes
            </Button>,
            <Button key="no" color="secondary" size="small"
              onClick={this.handleClose}>
                No
            </Button>,
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}>
              <CloseIcon />
            </IconButton>
          ]}
        />
        <Grid container spacing={0} justify="center">
          <Grid item xs={12} md={3}>
            <AppBar position="static">
              <Toolbar>
                <div className={classes.iconLeft}>
                  <img src={logo} className={classes.logoImg} alt="QuakeArts.com" />
                </div>
                <div className={classes.iconRight}>
                  <SvgIcon color="secondary">
                    <path fill="#000000" d="M20,20H7A2,2 0 0,1 5,18V8.94L2.23,5.64C2.09,5.47 2,5.24 2,5A1,1 0 0,1 3,4H20A2,2 0 0,1 22,6V18A2,2 0 0,1 20,20M8.5,7A0.5,0.5 0 0,0 8,7.5V8.5A0.5,0.5 0 0,0 8.5,9H18.5A0.5,0.5 0 0,0 19,8.5V7.5A0.5,0.5 0 0,0 18.5,7H8.5M8.5,11A0.5,0.5 0 0,0 8,11.5V12.5A0.5,0.5 0 0,0 8.5,13H18.5A0.5,0.5 0 0,0 19,12.5V11.5A0.5,0.5 0 0,0 18.5,11H8.5M8.5,15A0.5,0.5 0 0,0 8,15.5V16.5A0.5,0.5 0 0,0 8.5,17H13.5A0.5,0.5 0 0,0 14,16.5V15.5A0.5,0.5 0 0,0 13.5,15H8.5Z" />
                  </SvgIcon>
                </div>
              </Toolbar>
            </AppBar>
            <Card>
              <CardContent className={classes.cardForm}>
                <h3>Messages:</h3>
                <List>
                  {this.state.messages.map((sms,index) =>{
                    return [
                    <ListItem key={index}>
                      <ListItemIcon>
                        <SvgIcon>
                          <path fill="#000000" d="M17,19V5H7V19H17M17,1A2,2 0 0,1 19,3V21A2,2 0 0,1 17,23H7C5.89,23 5,22.1 5,21V3C5,1.89 5.89,1 7,1H17M9,7H15V9H9V7M9,11H13V13H9V11Z" />
                        </SvgIcon>
                      </ListItemIcon>
                      <ListItemText>{sms.message}</ListItemText>
                    </ListItem>,
                    <Divider />]
                    })}
                </List>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <Websocket url={"wss://demo.quakearts.com:8883/connection/"+mobileNo}
              ref ={this.websocket} 
              onMessage={this.handleData}
            />
      </div>
    );
  }
}

export default withStyles(styles)(App);
